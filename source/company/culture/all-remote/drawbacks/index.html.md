---
layout: markdown_page
title: "All Remote Drawbacks"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Introduction

Despite all of its [advantages](/company/culture/all-remote/benefits/), all-remote work isn't for everyone. It can have disadvantages for potential employees depending on their lifestyle and work preferences, as well as the organization. In the spirit of [transparency](/handbook/values/#transparency), we'll also highlight counterpoints and solutions to these challenges.

<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/CwOLAKSdlfs" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

In the [video](https://youtu.be/CwOLAKSdlfs) above, GitLab Director of Technical Evangelism [Priyanka Sharma](https://gitlab.com/pritianka) discusses pros and cons of remote working with a panel of experts from [TFiR](https://www.tfir.io/), [Arm](https://www.arm.com/) and [ISG Research](https://isg-one.com/research).

## For employees

<!-- blank line -->

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/xHPwHNhQ50A" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

In the [video](https://youtu.be/xHPwHNhQ50A) above, GitLab co-founder and CEO Sid Sijbrandij addresses remote work and its biggest challenges with [Leo Widrich](https://twitter.com/leowid), the co-founder of [Buffer](https://buffer.com/) and present-day executive coach.

  1. [Onboarding](/company/culture/all-remote/learning-and-development/#how-do-you-onboard-new-team-members) can be difficult when you're remote, because it involves [more self-learning](/company/culture/all-remote/learning-and-development/) and you're not physically with your new coworkers and fellow new hires.
    - Learn more about how GitLab [onboards its all-remote team members](/company/culture/all-remote/learning-and-development/#how-do-you-onboard-new-team-members).
  1. The first month in a remote role can feel [lonely](/blog/2018/04/27/remote-future-how-remote-companies-stay-connected/), especially if you're [transitioning from a traditional office setting](/blog/2018/03/15/working-at-gitlab-affects-my-life/).
    - To prevent loneliness, all-remote companies should consider an [intentional structure to informal communications](/company/culture/all-remote/informal-communication/).
    - Scheduling [coffee chats](/company/culture/all-remote/informal-communication/#coffee-chats), [social calls](/company/culture/all-remote/informal-communication/#social-calls), and [incentivizing in-person visits](/handbook/incentives/#visiting-grant) between team members are several of the ways in which GitLab accomplishes this.
  1. Remote settings can cause a breakdown in communication skills if organizations aren't deliberate about creating ways for their people to stay connected.
    - All-remote companies should default to asynchronous communication to combat feelings of being left out of important discussions.
    - We welcome all-remote companies to glean from [GitLab's approach to meetings](/company/culture/all-remote/meetings/), as well as our [scheduled AMAs, group conversations, and key meetings](/company/culture/all-remote/learning-and-development/#ask-me-anything-ama-group-conversations-and-key-meetings) that are open to all.
  1. Some may find it difficult to work in the same setting as they live and sleep, because a dedicated workspace helps to switch the context from their home life to work.
    - All-remote companies should not assume that team members will work from their home 100% of the time. Organizations can consider [reimbursing coworking space usage](/handbook/spending-company-money/) and [creating an atmosphere](/handbook/spending-company-money/) where team members are [encouraged to construct a workspace](/company/culture/inclusion/#fully-distributed-and-completely-connected) that is ideal for their comfort and productivity. 
    - GitLab team members have shared their own solutions in a number of blog posts, including a series on [working at home with kids](/blog/2019/08/01/working-remotely-with-children-at-home/) and [utilizing an RV as a traveling office](/blog/2019/06/25/how-remote-work-at-gitlab-enables-location-independence/).
1. Team members in different time zones may have to compromise on meeting times.
  - All-remote companies should consider [meetings](/company/culture/all-remote/meetings/) as a last resort, instead relying on asynchronous collaboration tools like Google Docs and [GitLab Issues](https://docs.gitlab.com/ee/user/project/issues/) to facilitate meaningful dialog without time zone concerns.
  - To prevent [pent-up frustration](/handbook/values/#five-dysfunctions) over working odd hours to facilitate a global call, all-remote companies should consider treating each team member as a [manager of one](/handbook/values/#managers-of-one). This goes beyond the basic definition of flexibility by empowering team members to structure each day according to the needs at hand. For example, spending more time with family earlier in the day to compensate for a late-night work call.
1. It can be hard to separate your personal and work life. It's important to encourage boundaries and make sure you don't continue to work during your family time.
    - [Preventing a culture of burnout starts at the top](/blog/2018/03/08/preventing-burnout/). In all-remote companies, it's important to reinforce this from the [interview process](/blog/2019/03/28/what-its-like-to-interview-at-gitlab/), to [onboarding](/company/culture/all-remote/learning-and-development/#how-do-you-onboard-new-team-members), to regular [1:1s](/handbook/leadership/1-1/).
    - All-remote companies should consider implementing a [Results value](/handbook/values/#results), where [results (as opposed to hours) are measured](/handbook/values/#measure-results-not-hours). Fundamentally, this requires organizational trust — believing that colleagues will do the right thing rather than implementing rigid rules.
    - At GitLab, we encourage team members to [communicate with their manager when they recognize burnout](/handbook/paid-time-off/#recognizing-burnout), and to be mindful of the last time a team member [took time off from work](/handbook/paid-time-off/#paid-time-off).
1. Remote work requires you to [manage your own time](/handbook/values/#managers-of-one) and be self-motivated, disciplined, and [organized](/blog/2019/06/18/day-in-the-life-remote-worker/).
    - All-remote companies tend to attract those who place a [high degree of value on autonomy](/blog/2018/03/15/working-at-gitlab-affects-my-life/). To some degree, companies who are hiring can expect applicants to opt-in to [this way of working](/blog/2019/07/09/tips-for-working-from-home-remote-work/).
    - [Screening employees](/blog/2019/03/28/what-its-like-to-interview-at-gitlab/) for these attributes is fairly easy [during the interview process](/blog/2017/01/03/how-to-prepare-for-a-virtual-interview/). Even if a prospective team member has yet to work in a remote setting, asking for examples of their mindset and performance while working from home, an event, or while traveling can provide valuable insight.
    - While each team member is different, you can get a glimpse of what it's like to operate in an all-remote setting through this [GitLab blog post](/blog/2019/06/18/day-in-the-life-remote-worker/), entitled *[A day in the life of the "average" remote worker](/blog/2019/06/18/day-in-the-life-remote-worker/)*.
1. Differences in currency and tax requirements around the world can create challenges for the employee.

## For your organization

  <!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/ha4aMKl3MRA" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

In the [video](https://youtu.be/ha4aMKl3MRA) above, GitLab co-founder and CEO Sid Sijbrandij discusses some of the organizational challenges involved in building and sustaining an all-remote company.

1. Because it's non-traditional, all-remote work sometimes [concerns](/blog/2018/10/18/the-case-for-all-remote-companies/) investors, partners, and customers.
    - All-remote companies are welcome to glean from [GitLab's template for keeping investors in the loop](/blog/2018/10/17/how-we-keep-investors-in-the-loop/).
    -  For those in the early stages of fundraising for an all-remote company, here are [30 tips from GitLab co-founder and CEO Sid Sijbrandij](/blog/2016/10/14/fundraising-tips-ceo/).
    -  For even more on this topic, [listen](/blog/2019/08/16/all-remote-fundraising/) to Sid unpack why venture firms struggle to fund all-remote startups with [Maren Kate](https://www.linkedin.com/in/marenkate), host of the [From 5 to 50 podcast](https://podcasts.apple.com/us/podcast/id1467214647).

> "In the beginning they assess your team, then they assess your product, and then they assess your financials. When it comes to the team, [investors are] super skeptical they will be able to create something with all-remote. Then when it’s about the product they say, ‘Yes, maybe, but what about scaling?’ And then when it’s about the financials you can let the numbers speak for themselves so it’s less of a concern. — *GitLab co-founder and CEO Sid Sijbrandij*

<iframe src="https://omny.fm/shows/from-5-to-50/from-5-to-50-podcast-episode-sid-sijbrandij/embed?style=cover" frameborder="0" width="100%" height="180"></iframe>
2. Differences in currency as well as tax, immigration, and labor laws around the world can create [compliance challenges](https://www.forbes.com/sites/forbeshumanresourcescouncil/2019/07/12/remote-work-is-here-to-stay-heres-how-to-avoid-three-common-compliance-issues/) for the organization.
    - While each organization is unique, we welcome other all-remote companies to learn from GitLab's [ever-evolving approach to hiring and remaining compliant across the globe](/company/culture/all-remote/hiring/).
3. You have to be more intentional about [cultivating](/blog/2019/06/04/contribute-wrap-up/), [sustaining](/company/culture/all-remote/learning-and-development/#ask-me-anything-ama-group-conversations-and-key-meetings), and [documenting](/company/culture/all-remote/learning-and-development/#the-importance-of-documenting-everything) your company culture.
    - While this is an obstacle for all-remote companies, colocated organizations should be deliberate about documenting their culture as well.
    - All-remote organizations should document everything, including [values](/handbook/values/), and ensure that they are easily accessible to all.
    - GitLab demonstrates this with its [Handbook](/handbook/), a living document that is [continually iterated on](/handbook/about/#count-handbook-pages) by team members. We encourage all-remote organizations to glean from this and develop their own handbook.

----

Return to the main [all-remote page](/company/culture/all-remote/).
