---
layout: job_family_page
Title: Finance Systems Analyst
---

The Finance Systems Analyst is the operational owner of the software applications in the finance and accounting department.
They maintain and optimize the integrations of the ecosystem and work directly with other departments where the tools intersect and the data passes from one system into another.

## Responsibilities
- Primary administrator of all finance software applications
- Drive financial system strategy and direction across Finance and coordinate with other teams such as Sales and Product
- Optimize, enhance, and implement financial applications such as Zuora (or replacement), Netsuite, Stripe, and Tipalti
- Collaborate with other teams to improve system integrations between financial applications and ADP, Expensify, Salesforce, Blackline, BambooHR, and TripActions
- Optimize current financial system processes (order processing, month close) to scale for growth and efficiency.
- Protects data integrity and where data quality is lacking, identify the root cause and address systematically with improved processes
- Facilitate successful implementation of new technology and processes, fostering an organization of continuous process improvement
- Creates and maintains system architectural diagrams and documentation in the handbook
- Manage data, security, and legal governance processes and adherence of guidelines between internal business units and systems in order to ensure efficient and compliant execution of financial management resources
- Provisions, deprovisions, and performs audits of users
- Train financial application users on new tools and changes to existing tools
- Create and manage project plans with clearly defined deliverables and resources, coordinate work streams and dependencies, track and communicate progress, and identify obstacles and ensure they are addressed

## Requirements
- Ability to use GitLab
- BA/BS degree
- 5+ years in a business and/or systems role
- Financial ecosystems expert including system rules, workflows, formulas, sandboxes, and customization of all standard and custom objects
- Demonstrated ability to drive and lead cross-functional teams to meet business objectives and influence colleagues, fostering strong working relationships
- Strong strategic thinking, creative problem solving and analytical skills
- Experience with Salesforce CRM, netsuite and understanding of CPQ
- Experience with ASC606 and Sox Compliance
- SaaS and B2B experience preferred
- Interest in GitLab and open source software
- You share our values and work in accordance with those values
- Ability to thrive in a fully remote organization

## Nice to Have
- MacOS experience
- Google Suite experience
- A love of open source
- Experience with SaaS products
- Experience using GitLab/Git

## Performance Indicators
- As with all roles in the Finance Department the Finance System Analyst participates in the [team KPIs](https://about.gitlab.com/handbook/business-ops/data-team/kpi-index/#sts=Finance%20KPIs).

## Hiring Process
- Selected candidates will be invited to schedule a [screening call](https://about.gitlab.com/handbook/hiring/#screening-call) with our Global Recruiters.
- Next, candidates will be invited to an interview with the Hiring Manager.
- Second round interviews will be the Interview team that consists of 2-4 interviews.
- An optional final interview with a C level Executive may occur.
- Additional details about our process can be found on our [hiring page](https://about.gitlab.com/handbook/hiring).
- Successful candidates will subsequently be made an offer via email.

