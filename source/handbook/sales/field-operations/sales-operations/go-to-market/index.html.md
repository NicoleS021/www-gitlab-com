---
layout: handbook-page-toc
title: "Go To Market"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Welcome to The GTM Page! 

The purpose of this page is to centralize and document all key Go To Market policies & links for ease of use.

**Territory Tables**
https://about.gitlab.com/handbook/sales/territories/ 

**Annual Total Addressable Market (ATAM)**
ATAM is a yearly exercise to ensure our systems reflect our Account Ownership Rules of Engagement. This exercise includes evaluating key data points (Location, Employee Count, etc) and as re-assigning the ownership of data accordingly. 

SFDC has a collection of fields labeled with the prefix “ATAM”
ATAM Segment
This field is automatically calculated based on the MAX headcount of all accounts in the account’s hierarchy. Ie if 1 account in the hierarchy is >=2,000, all accounts in the hierarchy will be considered “Enterprise”
Note: this may be different from the current “Sales Segment” due to historical data, exceptions (like local vs global ownership) or other business reasons. 

**Account ownership change request process**
To request an account ownership change (to you or to someone else), please follow the below process.

REQUEST
Enter your desired owner into “Requested Owner Name” field on Account. If request is for entire hierarchy, please make request on Ultimate Parent Account
Enter your rationale for move (please be as detailed as possible) in “Requested Owner Rationale” Field
SALES OPERATIONS REVIEW
On a weekly basis, Sales Operations will review all account change requests. 
SALES OPERATIONS RESPONSE
Approved: “Next Account Owner” & “Next Owner Date” will be filled in by Sales Ops. 
Reporting for pending moves in/out of your name here: REPORT LINK
Rejected: Based on the Rules of Engagement, this request cannot be completed, rejection rationale will be included. If you find this incorrect, please escalate to your direct sales manager
Approval/More Information Required: Rationale is incomplete or requires input from other party (current owner or Sales Management)
ACCOUNT OWNERSHIP UPDATED
SFDC will automatically change ownership of the Account, child Accounts & related contacts to new owner on the “Next Owner Date” 


**Rules of Engagement FY20**
https://about.gitlab.com/handbook/business-ops/resources/#account-ownership-rules-of-engagement

**(WIP)Rules of Engagement FY21**
https://gitlab.com/gitlab-com/sales-team/field-operations/sales-operations/issues/458

