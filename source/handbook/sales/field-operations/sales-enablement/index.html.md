---
layout: handbook-page-toc
title: "Sales and Customer Enablement"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## How to Communicate with Us

Slack: [#sales-and-customer-enablement-team](https://gitlab.slack.com/archives/sales-and-customer-enablement-team)

## Mission

Help customers successfully grow and evolve in their journey with GitLab to achieve positive business outcomes with effective enablement solutions aligned to [Gitlab’s values](/handbook/values/).

## Key Programs

*  [Customer Enablement](/handbook/sales/field-operations/customer-enablement/)
*  [Sales Onboarding](/handbook/sales/onboarding/)
*  [GitLab Command of the Message](/handbook/sales/command-of-the-message)
*  [Continuous Improvement / Continuous Development for Sales (CI/CD for Sales)](/handbook/sales/training/)

## Handbook-First Approach to GitLab Learning and Development Materials

Chat between David Somers (Director, Sales and Customer Enablement) and Sid Sijbrandij (CEO)
<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/oXTZQpICxeE" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

### Key Discussion Points

*  Our [Mission](/company/strategy/#mission) is that Everyone Can Contribute, and our most important value is [Results](/handbook/values/#results). Like we've extended that to the Handbook, we want to extend it to our Learning Materials.
*  We want to leverage the best of an e-learning platform, with the benefits of reminders, interactivity, and more but make sure the materials we produce are also available to those who aren't using an e-learning platform, while fulfilling [our mission](/company/strategy/#mission). 
*  There are benefits to keeping our e-learning material [handbook-first](/handbook/handbook-usage/#why-handbook-first):
   * Folks who have already completed a formal training through an e-learning platform may want to return to the materials
   * Those who never go through the formal platform may also benefit from the materials
   * The handbook continues to be the SSOT, with the e-learning platform leveraging handbook materials through screenshots, embeds, and more

## Six Critical Questions
inspired by _The Advantage: Why Organizational Health Trumps Everything Else in Business_ by Patrick Lencioni

1. **Why does the GitLab Sales and Customer Enablement team exist?**
See our [Mission](/handbook/sales/field-operations/sales-enablement/#mission)
1. **How do we behave?**
On our best day, we show up with a positive attitude while demonstrating [GitLab’s values](handbook/values/) along with the following behaviors to overcome the [Five Dysfunctions](/handbook/values/#five-dysfunctions):
      - **Trust**: Extend trust, actively listen, and assume noble intent; give and receive feedback with respect and solicit feedback often
      - **Embrace Healthy Conflict**: Engage in constructive conflict for the purpose of achieving shared goals & objectives; resolve personal issues, quickly and directly
      - **Commitment**: Support decisions once decisions are made with a GitLab team-first approach
      - **Accountability**: Hold ourselves and each other accountable while encouraging each other & celebrating successes
      - **Results**: Strong drive for results and a focus on the customer; demonstrate passion for continuous learning & improvement
1. **What does the Sales and Customer Enablement team do?**
   * Define, coordinate, and/or lead the development and delivery of effective enablement solutions (training, technology, knowledge, content, process, and tools) for GitLab sales reps, Customer Success professionals, partners, and customers
   * Lead facilitation of prioritized behavior change in the GitLab field organization
   * Collaborate cross-functionally with key stakeholders including Sales, Customer Success, Marketing, Sales Ops, Product, and PeopleOps
1. **What does success look like?**
The below is a work in progress as we define success measures across each stage of the customer journey:
   *  **Awareness** - Sales Development Rep: TBD
   *  **Education** - Sales roles: Lead to opportunity conversion rate; Time for new hire to deliver GitLab pitch
   *  **Selection** - Sales & Solution Architect roles: Win rate; Average sales cycle duration; Time for new hire to close first deal
   *  **Onboarding** - Technical Account Manager: TBD
   *  **Use & Expansion** - Sales roles and Technical Account Manager: Renewal rate
1. **What is most important right now (2HFY20)?**
   *  **General**
      - Strategic alignment to and enlist support from field leadership
      - S&CE organizational health 
      - S&CE collaboration (“swarm model”)
   *  **Customer Enablement** 
      - Medium to long-term strategy development 
         - Document current state
         - Define desired future state
         - Assess gaps
         - Prioritize areas of focus for short, medium, and long-term time horizons
         - Align on strategy with key leaders
         - Build & execute tactical plans to support the agreed-upon strategy
         - Conduct regular review cadence to continually assess and refine strategic priorities and execution plans  
   *  **Sales & Customer Success Enablement**
      - Execute against short-term priorities
         - Accelerate time to value of new hire sales team members via operational excellence and ongoing improvement to Sales Onboarding
         - Operationalize Command of the Message and MEDDPICC across the global field organization
         - Other (TBD)
      - Medium to long-term strategy development for Enterprise Sales, Commercial Sales, and Customer Success
         - Document current state  
         - Define desired future state
         - Assess gaps
         - Prioritize areas of focus for short, medium, and long-term time horizons
         - Align on strategy with key leaders
         - Build & execute tactical plans to support the agreed-upon strategy
         - Conduct regular review cadence to continually assess and refine strategic priorities and execution plans
   *  **Partner Enablement**
      - Embrace partner enablement as an extension of Sales, Customer Success, and Customer enablement
      - Assess how we can support short to medium-term partner enablement priorities for Public Sector and authorized resellers (particularly as an extension of internal Sales & Customer Success enablement efforts) while we wait for GitLab WW VP of Channel Sales leader to join the company and outline her strategy 
1. **Who must do what?**
   *  **Director, Sales & Customer Enablement**
      - Create a clear vision for the future that connects the Sales & Customer Enablement (S&CE) vision & mission to the GitLab and GitLab Sales enablement
      - Oversee direction of current & future portfolio of S&CE programs
      - Help the team prioritize, where appropriate
      - Empower, trust, and support team members to develop strategies & tactics to achieve the vision
      - Remove obstacles to achieving that vision
      - Motivate and inspire team members and set the tone for team norms
      - Build & maintain meaningful influence across GitLab
      - Lead global Sales Kick Off (SKO) strategy planning and execution efforts with cross-functional team (including Sales, Corporate Marketing, Product Marketing, and more)
      - Expand professional knowledge and subject matter expertise by attending workshops/training, networking, and reading relevant publications, blogs, books, etc.
   *  **Program Managers: Enterprise Sales, Commercial Sales, and Technical Sales / Customer Success**
      - Collaborate with and influence key stakeholders to successfully 
         - Launch global S&CE programs on time and on budget
         - Build and maintain effective relationships between S&CE and regional Sales & Customer Success leaders
         - Develop and maintain a short-term (3-6 months), medium-term (12 months), and long-term (18-24 months) roadmap & vision
         - Develop & maintain role-based learning journeys outlining what each key role needs to know, do, say, think, and show to be successful at GitLab
         - Apply change management best practices to optimize program awareness, desire, knowledge & ability, learner reinforcement, and program sustainment
         - Measure, track, analyze, report, and act on agreed-upon success measures to continuously improve program performance
         - Identify ongoing integration opportunities with other GitLab sales enablement initiatives (within and outside our team)
      - Expand professional knowledge and subject matter expertise on your program/s by attending workshops/training, networking, and reading relevant publications, blogs, books, etc.
   *  **Sales Training Facilitator**
      - Successfully deliver and/or facilitate the delivery of key virtual and in-person Sales & Customer Success trainings including but not limited to Sales QuickStart, Command of the Message & MEDDPICC workshops, and weekly sales enablement videocasts
      - Collaborate with and support S&CE Program Managers
      - Design and deliver prioritized Google Classroom learning paths
      - Expand professional knowledge and subject matter expertise on sales training delivery & facilitation by attending workshops/training, networking, and reading relevant publications, blogs, books, etc.
   *  **Solutions Manager, Customer & Partner Education**
      - Collaborate with and influence key stakeholders to successfully
         - Define, scope, and prioritize GitLab’s external training offerings, including persona and market segmentation analysis, offering definition, scope, and pricing
         - Develop and deliver prioritized global training offerings, content, and sales & marketing collateral
         - Ensure training delivery model is focused on customer success outcomes while championing quality and efficiency
         - Measure and report on the effectiveness of training enablement investments and programs
         - Ensure a robust closed feedback loop that embraces continuous improvement and iteration
         - Identify and act on opportunities to improve the customer experience via innovative training offerings
         - Build business case for additional training and enablement resources as needed
      - Collaborate with Program Managers (especially Program Manager for Technical Sales / Customer Success) to identify and leverage synergies
      - Expand professional knowledge and subject matter expertise on customer education/training and certification by attending workshops/training, networking, and reading relevant publications, blogs, books, etc.

## Sales and Customer Enablement groups, projects, and labels
   *  **Groups**
      - Use the GitLab.com group for epics that may include issues within and outside the Sales Team group
      - Use the GitLab Sales Team group for epics that may include issues within and outside the Field Operations group
   *  **Projects**
      - Create issues under the “Enablement” project
   *  **Labels**
      - **Team labels**
          - `sales and customer enablement` - issue initially created, used in templates, the starting point for any label that involved Sales and Customer Enablement
          - `FieldOps` - use this label for issues that we want to expose to the VP of Field Operations; these will often mirror issues with the SCE: priority 1 tag, particularly those relating to OKRs and other prioritized projects by Field Ops leadership
      - **Stakeholder/Customer labels**
          - `SCE:sales enablement` - label for Sales and Customer Enablement issues related to enabling Sales roles
          - `SCE:CS enablement` - label for Sales and Customer Enablement issues related to enabling Customer Success (CS) roles
          - `SCE:customer enablement` - label for Sales and Customer Enablement issues related to enabling GitLab customers
      - **Initiative labels**
          - `onboarding` - label for issues related to onboarding
          - `continuing education` - label for issues related to continuing education
          - `career dev` - label for issues related to career development
          - `force management` - label for issues related to Force Management engagement
          - `sales enablement sessions` - label for weekly virtual sales enablement series
          - `sko` - label for issues related to Sales Kick Off
          - `SCE:new request` - label for new requests originating outside of the Sales and Customer Enablement team
          - `status:plan` - used in conjunction with sales enablement sessions to indicate when a session topic has been prioritized but not yet scheduled
          - `status:scheduled` - used in conjunction with sales enablement sessions to indicate when a session topic has been prioritized and scheduled
      - **Priority labels**
          - `SCE priority::1` - Home runs (high value to GitLab and high likelihood of success that align to S&CE OKRs) and committed to completion within 90 days. This category will be limited because not everything can be a priority. 
          - `SCE priority::2` - Big Bets (high value to GitLab, lower likelihood of success) within 90 days
          - `SCE priority::3` - Small wins (lower value to GitLab, high likelihood of success) within 90 days
          - `SCE priority::Backlog` - Things in the queue not currently being worked
          - `QBR` - Requests from Sales QBRs
   *  **Boards**
      - [Sales and Customer Enablement Board](https://gitlab.com/groups/gitlab-com/sales-team/-/boards/1191445?&label_name[]=sales%20and%20customer%20enablement)
      - [Sales Enablement Sessions Board](https://gitlab.com/groups/gitlab-com/sales-team/-/boards/1231617?&label_name[]=sales%20enablement%20sessions)
