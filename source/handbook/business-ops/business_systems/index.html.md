---
layout: handbook-page-toc
title: "Business System Analysts"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Business Systems Analyst)

### Links
*  [Job Description](/job-families/finance/business-system-analyst/)
*  [Epic Board: Business Operations](https://gitlab.com/groups/gitlab-com/business-ops/-/epics?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=BSA)

### General Ongoing
*  Involvement with evaluating tools: We can provide [templates](https://drive.google.com/open?id=18b0MYkPYFAzBn3tXB2DbMOrBY3zPRLQa) for vendor "Request for Proposals" and user stories; we can help review your user stories.
We are also happy to be available for demos and would like to participate in tool evaluation that integrates with other tools or intersects with multiple departments.
   *  [Template: Request for Proposal](https://docs.google.com/document/d/1_Q2b5opYUQ9TlGmF2vOJ6anu0spVFMkNO6YCR4UjYXM/edit?usp=sharing)
   *  [Template: User Stories](https://docs.google.com/spreadsheets/d/1c1R0pqKr8YwXXATzFVEUaofF2luNrHbmcNkKAWisebs/edit?usp=sharing)

### Current High Level Work
Karlia Kue
*  Scale Onboarding
*  Offboarding compliance
*  Company Tool Stack: Inventory, Information

Jamie Carey
*  Portal Analysis: system integrations, GitLabber user stories, identifying pain points of all departments
*  Scale Access Requests + Access Request Role Templates (Working with IT Ops/Help)