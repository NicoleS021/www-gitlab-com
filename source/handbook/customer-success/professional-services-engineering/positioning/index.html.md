---
layout: handbook-page-toc
title: "Positioning Professional Services"
---
# Positioning Professional Services
{:.no_toc}

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Value sell for Professional Services

### Elevator Pitch

**The Problem - Customer Perspective**

The customer has decided that GitLab is their tool of choice going forward, but has concerns about realizing the value of GitLab quickly and seamlessly.  These concerns can be in various areas:

* Lack of subject matter expertise on the GitLab technology stack
* Lack of adequate resources to promptly stand up GitLab
* Lack of sufficient resources to stand up GitLab in a highly-available or geographically dispersed way
* Concern around a large number of users coming off of legacy systems
* Concerns about change management for new processes
* Lack of confidence in the ability to deliver on the transformative promise of adopting "all" of GitLab

**Solution**

### Single Sentence

GitLab Professional Services helps organizations reduce time to market by accelerating the adoption of modern software delivery methods.

### Short Message

"The whole is greater than the sum of its parts" - and this is particularly true in modern software development. GitLab enables all aspects intrinsic to software delivery, and we provide specialized training in these practices, such as CI/CD, version control, metrics, and more.

### Long Message

Adopting GitLab gives you the best-of-breed Concurrent DevOps tool on the market while Professional Services will help you also align your people and processes to match.

Our Professional Services team is made up of not only GitLab subject matter experts but seasoned DevOps professionals who have experience in deploying and maintaining both large-scale applications as well as creating and teaching best practices throughout the SDLC.  Our experts help lead Concurrent DevOps Transformations, providing direct support to our customer’s strategic business initiatives.  Their experience with other Fortune 500 enterprises allows you to crowd-source your enterprise’s digital transformation.

GitLab's Professional Services team exists to enable you to realize the full value of your GitLab installation.  We can provide direct implementation support to ensure your GitLab installation is resilient and secure.  We also offer migration services to facilitate your transition to GitLab by delivering a clean dataset to resume operations at once.   Our education and specialized training provide training in best practices, such as CI/CD, version control, metrics, and more.

## Sales Collateral 

### Pitch Deck

To discuss our services offerings with prospects, it is often helpful to have a few slides to describe the role of the professional services team.  Feel free to use this deck directly - however if you'd like to modify it please first make a copy.

[Professional Services Pitch Deck](http://bit.ly/psslides)

### Data Sheets

Professional Services Data Sheets are available as subpages to the marketing site. You can find them through the [Professional Services portal](/services/).

## Services Calculator

The goal of the services calculator is to provide the sales team with the ability to self-serve and create a ROM package for a customer with the minimal amount of information.  You can get access to the service calculator [here](https://services-calculator.gitlab.io/).

## Other Collateral

For all other sales collateral documentation, see the [Professional Services Sales Enabelment folder](https://drive.google.com/drive/u/0/folders/1vLhSdmlwClou_16I1SU9d3X0oG1EtBHv) on Google Drive.
