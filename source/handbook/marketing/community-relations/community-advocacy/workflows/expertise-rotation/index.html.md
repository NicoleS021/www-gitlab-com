---
layout: handbook-page-toc
title: "Expertise rotation workflow"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Overview
Community Advocates have their own expertises (EDU/OSS/Merch/Integrations) and we need to make sure that each team member is always up to date and ready to jump in or tackle the different expertise workflow/question/request.

## Regular daily workflow
The CA team consists of 3 team members, where 1 is located in the US and the rest is in the EU timezone.

* Combined in the EU, one expert starts the day by going through EDU/OSS requests while the other is handling important Zendesk tickets as HackerNews, Website comments and Twitter.
* After the important views are cleared, the swag expert handles the Merchandise view and the EDU/OSS expert picks anything left.
* A team member located in the US starts the day by getting updated during the calls and going through Zendesk tickets.

## Rotation workflow
Consider switching the day to day workflows between EDU/OSS and Merchandise experts.

Example rotation:
* The recommended rotation length is 10 work days (feel free to make it longer if you think that's required).
* Combined in the EU, the swag expert starts the day by going through EDU/OSS requests while the other is handling important Zendesk tickets as HackerNews, Website comments and Twitter.
* After the important views are cleared, the EDU/OSS expert handles the Merchandise view and the swag expert picks anything left.
* A team member located in the US starts the day by getting updated during the calls and going through any important Zendesk ticket left.
* After the US team member is on top of everything, they should continue with the tickets in EDU/OSS/Merchandise views.
* Make sure to update the handbook with everything new that you've learned and it's missing from the handbook.


## Training
Advocates should do training sessions during the Community Advocacy daily planning calls when there are no any other important topics to discuss. 