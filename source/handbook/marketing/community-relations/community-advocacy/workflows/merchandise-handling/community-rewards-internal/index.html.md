---
layout: handbook-page-toc
title: "Internal community contributions swag"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Let's award contributors together

Each team member should be able to send some swag to anyone in the community who deserves it. We'll be using Printfection's giveaway links to obtain this.

## Eligibility
 
Anyone who wrote the blog post, tip & trick or made a video tutorial about the GitLab is eligible for the prize. 

Please update this section with other examples of contributions as we want to include everybody!

## Giveaway links

The list of the giveaways is found in the [Google Sheets file](https://docs.google.com/spreadsheets/d/1kzK25SE9jPXx50Lw0W8jtm9u77Zr4w13FAg1gyD6Egk/edit?usp=sharing) editable only by GitLabbers.

## Sending the swag

Once you've decided to award the community contributor, please open the [sheet file](https://docs.google.com/spreadsheets/d/1kzK25SE9jPXx50Lw0W8jtm9u77Zr4w13FAg1gyD6Egk/edit?usp=sharing) and follow these rules:

* Pick the first available (not used or sent) link in the row.
* Fill the 'Sent to Name' field with the contributor full name.
* [Optional] Fill the 'Sent to Email' field with the contributor email address.
* Fill the Requester field with your name.
* Reach out to contributor with the giveaway URL and let them know they just need to pick the item and input their shipping details.

Note: This data is required for advocates to monitor and check the placed orders within Printfection.

## Include everyone

If you think that someone deserves a swag prize, feel free to award them with the link.
