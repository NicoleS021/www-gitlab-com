---
layout: handbook-page-toc
title: "Meltano Handbook"
---

[Meltano](https://meltano.com) enables anyone with access to SaaS APIs and spreadsheets to generate dashboards summarizing the status of their business operations.

[View our Roadmap](https://meltano.com/docs/roadmap.html) to learn more about our Mission, Vision, Persona, Focus, Business Model, and Cadence.

## On this page
{:.no_toc}

- TOC
{:toc .toc-list-icons}

## <i class="fas fa-book fa-fw color-orange font-awesome" aria-hidden="true"></i> Handbooks
{: #handbooks}

- [Product](/handbook/meltano/product/)
- [Engineering](/handbook/meltano/engineering/)
- [Marketing](/handbook/meltano/marketing/)

## Team

- [Danielle](/company/team/#dmor) - General Manager
- [Douwe](/company/team/#DouweM) - Engineering Lead
- [Micaël](/company/team/#mbergeron) - Sr. Backend Engineer
- [Yannis](/company/team/#iroussos) - Sr. Backend Engineer
- [Derek](/company/team/#derek-knox) - Sr. Frontend Engineer
- [Ben](/company/team/#bencodezen) - Sr. Frontend Engineer

## Community

We believe in building in public, and you can follow along with our progress:
- [Meltano blog](https://meltano.com/blog/)
- [Meltano YouTube channel](https://www.youtube.com/channel/UCmp7zJAZEC7I_n9BEydH8XQ?view_as=subscriber)
- [Meltano on Twitter](https://twitter.com/meltanodata)
- [Meltano community Slack](https://meltano.slack.com)

### Live Chat on Intercom.io

The [Meltano.com website](https://www.meltano.com) is set up with live chat powered by Intercom. When a website visitor to asks starts a conversation it will post in the [Meltano Slack instance](https://meltano.slack.com) in the #support-intercom channel, and Meltano core team members and community members can reply directly from there.

#### Intercom Accounts

Meltano team members each have basic accounts, and there is a shared account for whoever is on chat duty to log into with full access to the Inbox. Login credentials can be found in 1Password.
