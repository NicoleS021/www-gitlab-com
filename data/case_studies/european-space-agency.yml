title: European-Space-Agency
cover_image: '/images/blogimages/ESA_case_study_image.jpg'
cover_title: |
  Connecting the cosmos with Earth - How the European Space Agency uses GitLab to focus on space missions
cover_description: |
  ESA is improving the way it approaches mission operations software creation. GitLab is allowing teams to cross borders, increase cooperation and reshape working culture.
twitter_image: '/images/blogimages/ESA_case_study_image.jpg'

customer_logo: '/images/case_study_logos/esa-logo.svg'
customer_logo_css_class: brand-logo-tall
customer_industry: Technology
customer_location: France, Netherlands, Italy, Germany, Spain, United Kingdom, Belgium
customer_employees: 5,000
customer_overview: |
  The European Space Agency (ESA) provides a European-wide Space Programme. ESA's programmes are designed to find out more about Earth, its immediate space environment, our Solar System and the Universe, as well as to develop satellite-based technologies and services, and to promote European industries. ESA also works closely with space organizations outside Europe.
customer_challenge: |
  ESA shapes the development of Europe’s space capability and ensures that the investment in space continues to deliver results and benefits to the citizens of Europe and the world. The organisation looks to the stars, but faces the challenges of geographical separation, long-term software redundancy requirements, and many different software customer needs.

key_benefits:
  - |
    Code deploy speed is now in a matter of minutes when it took weeks just a few years ago
  - |
    The toolchain has been simplified with the usage of GitLab CI
  - |
    A culture of collaboration has bloomed and is increasing around the organization
  - |
    Synergy exploitation is increased

customer_stats:
  - stat: ~15%
    label: of ESA employees are actively using GitLab
  - stat: "140"
    label: groups, over 1500 projects
  - stat: 60k+
    label: jobs the first year using GitLab CI

customer_study_content:
  - title: the customer
    subtitle: Leading the charge as Europe’s gateway to space- The European Space Agency enables European space research and technology and space applications.
    content:
      - |
        The European Space Agency (ESA) is responsible for coordinating the financial and intellectual resources of its 22 member states
        to ensure that investment in space continues to deliver benefits to the citizens of Europe and the world. The organization focuses
        on a wide variety of missions focused on space exploration and continued research. Some of their recent missions include
        sending orbiters to Mercury and studying hypervelocity stars in the Milky Way.



  - title: the challenge
    subtitle: Without a central version control system in place, opportunities for collaboration, synergies and multiple exploitations of effort were less visible.
    content:
      - |
        The European Space Agency has always carefully deployed new technologies. Validation and security have been at the fore,
        and there is less drive to invoke newer practices or technologies for their own sake unless they clearly bring added
        benefits for ESA’s core business. This sometimes resulted in using older, trusted tools to share code, at the expense of timeliness.
      - |
        In 2015 different teams within ESA were using a heterogeneous approach to control systems, some examples were Subversion or CVS.
        The emergence of Git, and its subsequent adoption by the ESA IT Department was a harmonious intersection of user needs and secure
        technology. GitLab was validated and adopted for the European Space Agency as a code repository platform in 2016. Usage was initially
        limited to a hand-picked group of first-wave users, but demand quickly escalated.
      - |
        In just two years, more than 140 groups adopted GitLab as their software versioning tool. Across ESA, more than 1500 software projects
        have been created. These range from mission control systems, onboard software for spacecraft, image processing and monitoring tools for
        Labs. The ESA IT Department also uses GitLab to host their code tools and configurations infrastructure.


  - blockquote: “I think our use case is quite interesting at ESA. Our team develops software, and we are also flying spacecraft. So developing software is in addition to our main activity. There are a lot of tools that we
        require to support us in our actual job. In that sense, we want to minimize the time that we spend to build these tools. Furthermore,
        GitLab has also shown that it is very effective in sharing our tools with other teams.”
    attribution: Bruno Sousa
    attribution_title: Spacecraft Operations Manager at ESA



  - title: the solution
    subtitle: Creating a culture where collaboration is contagious because sharing is so easy
    content:
      - |
        GitLab was introduced to the ESA population in 2016. Teams across Europe embraced the tool at all ESA Establishments, and sites.
        They can now collaborate and share code and insights both within their teams and with other teams. The process is faster, in real time,
        and produces reliable, stable results. Users can use more of their time to focus on their mission-critical tasks and spend less time keeping
        tools running.
      - |
        The adoption rate was high. Within one week 40 projects were running in GitLab. “Right now, we have 15% of our user population using GitLab”,
        a representative from the ESA IT Department GitLab project commented.
      - |
        For ESA, this represents a departure from the previous software development culture. In the past, it was assumed that there were fewer
        synergies to be exploited. Version control systems were individualised, or teams had not implemented them. As the technology now matches
        ESA’s needs, GitLab lets ESA approach more standardisation and efficiency.

  - blockquote: “To see people collaborating is contagious. People are saying, ‘Hey, I want to have a project like that!’ or ‘I want to participate.’ These kinds of things can happen because everybody has access,”
    attribution: Redouane Boumghar
    attribution_title: Research Fellow at ESA

  - content:
      - |
        Initially, ESA implemented GitLab to exploit the version control capabilities within the tool. However, the user community has also benefitted
        from the Continuous Integration (CI/CD) capabilities within GitLab. ESA began exploiting CI/CD capabilities in November 2017, and there are
        currently 60,000 jobs in GitLab. Feedback to the ESA IT Department from the user community has conveyed the user’s satisfaction with this development.
        One user reported “We initially started using version control, but we discovered that we could use CI on our project. We tried it out and were immensely
        impressed with how well it all worked together”.
      - |
        ESA’s diversity of teams and tasks provides unique challenges. Bruno Sousa explains why his tasks require CI/CD saying, “For our use case, in particular,
        the CI/CD capabilities are extremely important. In my role I simultaneously responsible for flying a spacecraft and developing a tool for us, and also
        potentially for other missions. I don’t have the time to deploy the software over and over again, so GitLab is very helpful in facilitating the whole
        process. It makes everything easier so that I can focus on my core task of flying the spacecraft.”



  - title:  the results
    subtitle: Increasing excitement and speed around code deployments with innovation as a result
    content:
      - |
        GitLab has provided a software development turnaround speed that ESA had not previously been able to achieve. The code is now continuously deployed
        in a matter of minutes, when previously it may have taken weeks.
      - |
        GitLab is able to address challenges along many stages of the software development pipeline. In the past different ESA teams were using a variety of
        CI/CD engines. Now they are being replaced with GitLab CI because GitLab is more user-friendly. As more users move to GitLab, ESA’s obligation to
        maintain other tools is removed. GitLab CI is then integrated into more version control systems.
      - |
        The automation in GitLab also saves ESA IT Department resources. With operation and backup fully automated, IT Specialists can focus on monitoring the
        tool and importantly, addressing more IT challenges for the European Space Agency.
